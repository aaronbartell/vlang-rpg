     H NOMAIN
     H AlwNull(*UsrCtl)
     H BNDDIR('QC2LE')

      /copy vos_h
      /copy virt_h
      /copy vpase_h
      /copy vconv_h


       // *************************************************
       // template
       // *************************************************
       dcl-ds iconv_t qualified template; 
         rtn INT(10); 
         cd INT(10) DIM(12); 
       end-ds;

       dcl-ds qtqCode_t qualified template; 
         qtqCCSID INT(10); 
         qtqAltCnv INT(10); 
         qtqAltSub INT(10); 
         qtqAltSft INT(10); 
         qtqOptLen INT(10); 
         qtqMixErr INT(10); 
         qtqRsv INT(20); 
       end-ds;

       dcl-ds ciconv_t qualified template; 
         conviok CHAR(4); 
         conv likeds(iconv_t); 
         tocode likeds(qtqCode_t); 
         fromcode likeds(qtqCode_t); 
       end-ds;

       dcl-ds over_t qualified template; 
         ubufx CHAR(32);
         bytex char(1) overlay(ubufx);
         boolx ind overlay(ubufx);
         twox char(2) overlay(ubufx);
         uchrx uns(3) overlay(ubufx);
         shortx int(5) overlay(ubufx);
         ushortx uns(5) overlay(ubufx);
         intx int(10) overlay(ubufx);
         uintx uns(10) overlay(ubufx);
         longlongx int(20) overlay(ubufx);
         ulonglong uns(20) overlay(ubufx);
         floatx float(4) overlay(ubufx);
         double float(8) overlay(ubufx);
         ptrx pointer overlay(ubufx);
         char1 char(1) overlay(ubufx);
         char2 char(2) overlay(ubufx);
         char3 char(3) overlay(ubufx);
         char4 char(4) overlay(ubufx);
         char5 char(5) overlay(ubufx);
         char6 char(6) overlay(ubufx);
         char7 char(7) overlay(ubufx);
         char8 char(8) overlay(ubufx);
         char9 char(9) overlay(ubufx);
         char10 char(10) overlay(ubufx);
         char11 char(11) overlay(ubufx);
         chardim char(1) dim(32) overlay(ubufx);
       end-ds;

       // *************************************************
       // global
       // *************************************************
       DCL-C CNVOPNMAX CONST(128);
       DCL-C CNVOPNOK CONST('okok');

       DCL-C CNV_IN CONST(1);
       DCL-C CNV_OUT CONST(2);

       dcl-ds myiConv likeds(ciconv_t) dim(CNVOPNMAX);

       // *************************************************
       // convert APIs -- operating system
       // *************************************************
       DCL-C QP2_2_ASCII CONST('QTCPASC');
       DCL-C QP2_2_EBCDIC CONST('QTCPEBC');

       dcl-pr Translate EXTPROC('QDCXLATE');
         Length PACKED(15:5) CONST;
         Data VARCHAR(32766);
         Table CHAR(10);
       end-pr;

       dcl-pr iconvOpen likeds(iconv_t) EXTPROC('QtqIconvOpen');
         tocode likeds(qtqCode_t);
         fromcode likeds(qtqCode_t);
       end-pr;

       dcl-pr iconv INT(10) EXTPROC('iconv');
         hConv likeds(iconv_t) value;
         pInBuff POINTER VALUE;
         nInLen POINTER VALUE;
         pOutBuff POINTER VALUE;
         nOutLen POINTER VALUE;
       end-pr;

       dcl-pr iconvClose EXTPROC('iconv_close');
         cd likeds(iconv_t);
       end-pr;


       // *************************************************
       // convert APIs -- custom
       // *************************************************
       dcl-pr convOpen INT(10);
         fromCCSID INT(10);
         toCCSID INT(10);
         conv likeds(ciconv_t);
       end-pr;

       dcl-pr convClose INT(10);
         conv likeds(ciconv_t);
       end-pr;

       dcl-pr convCall INT(10);
         conv likeds(ciconv_t);
         buffPtr POINTER;
         buffLen INT(10);
         outPtr POINTER;
         outLen INT(10);
       end-pr;

       dcl-pr bigTrim UNS(20);
         start POINTER VALUE;
         len UNS(20) VALUE;
       end-pr;

       dcl-pr IleCharPaseString INT(10);
         flag INT(10) VALUE;
         argv POINTER VALUE;
         ilev POINTER VALUE;
         ileDesc likeds(virtVar_t);
         paseDesc likeds(virtVar_t);
         ileCCSID INT(10);
         paseCCSID INT(10);
       end-pr;

       dcl-pr IleInt10PaseInt10 INT(10);
         flag INT(10) VALUE;
         argv POINTER VALUE;
         ilev POINTER VALUE;
         ileDesc likeds(virtVar_t);
         paseDesc likeds(virtVar_t);
       end-pr;

       dcl-pr IleInt10PaseFloat8 INT(10);
         flag INT(10) VALUE;
         argv POINTER VALUE;
         ilev POINTER VALUE;
         ileDesc likeds(virtVar_t);
         paseDesc likeds(virtVar_t);
       end-pr;

       dcl-pr IleFloat8PaseFloat8 INT(10);
         flag INT(10) VALUE;
         argv POINTER VALUE;
         ilev POINTER VALUE;
         ileDesc likeds(virtVar_t);
         paseDesc likeds(virtVar_t);
       end-pr;

       dcl-pr IleFloat8PaseInt10 INT(10);
         flag INT(10) VALUE;
         argv POINTER VALUE;
         ilev POINTER VALUE;
         ileDesc likeds(virtVar_t);
         paseDesc likeds(virtVar_t);
       end-pr;

       dcl-pr IlePackedPaseFloat8 INT(10);
         flag INT(10) VALUE;
         argv POINTER VALUE;
         ilev POINTER VALUE;
         ileDesc likeds(virtVar_t);
         paseDesc likeds(virtVar_t);
       end-pr;

       dcl-pr IlePackedPaseInt10 INT(10);
         flag INT(10) VALUE;
         argv POINTER VALUE;
         ilev POINTER VALUE;
         ileDesc likeds(virtVar_t);
         paseDesc likeds(virtVar_t);
       end-pr;

       dcl-pr IleZonedPaseFloat8 INT(10);
         flag INT(10) VALUE;
         argv POINTER VALUE;
         ilev POINTER VALUE;
         ileDesc likeds(virtVar_t);
         paseDesc likeds(virtVar_t);
       end-pr;

       dcl-pr IleZonedPaseInt10 INT(10);
         flag INT(10) VALUE;
         argv POINTER VALUE;
         ilev POINTER VALUE;
         ileDesc likeds(virtVar_t);
         paseDesc likeds(virtVar_t);
       end-pr;

       // *************************************************
       // convert open
       // return (>-1 - good, <0 - error)
       // *************************************************
       dcl-proc convOpen;
         dcl-pi *N INT(10);
          fromCCSID INT(10);
          toCCSID INT(10);
          conv likeds(ciconv_t);
         end-pi;
         DCL-S rc INT(10) INZ(0);
 
         Monitor;
           // qtqCode_t:
           //             to         from
           // qtqCCSID    0 - job    PaseCCSID
           // qtqAltCnv   0 - na     0 - IBM default
           // qtqAltSub   0 - na     0 - not returned to initial shift state
           // qtqAltSft   0 - na     0 - substitution characters not returned
           // qtqOptLen   0 - na     0 - inbytesleft parameter must be specified
           // qtqMixErr   0 - na     0 - no error dbcs
           // qtqRsv      0 - na     0 - na
           memset(%addr(conv.tocode):0:%size(conv.tocode));
           memset(%addr(conv.fromcode):0:%size(conv.fromcode));
           memset(%addr(conv.conv):0:%size(conv.conv));
   
           // If unsuccessful, QtqIconvOpen() returns -1 
           // and in the return value of the conversion 
           // descriptor and sets errno to indicate the error.
           conv.fromcode.qtqCCSID = fromCCSID;
           conv.tocode.qtqCCSID = toCCSID;
           conv.conv = iconvOpen(conv.tocode:conv.fromcode);
           if conv.conv.rtn < 0;
             rc = -1;
           endif;
  
         On-error;
           rc = -1;
         Endmon;
 
         if rc < 0;
           memset(%addr(conv.tocode):0:%size(conv.tocode));
           memset(%addr(conv.fromcode):0:%size(conv.fromcode));
           memset(%addr(conv.conv):0:%size(conv.conv));
         endif;
 
         return rc;
       end-proc; 

       // *************************************************
       // convert close
       // return (>-1 - good, <0 - error)
       // *************************************************
       dcl-proc convClose;
         dcl-pi *N INT(10);
          conv likeds(ciconv_t);
         end-pi;
         DCL-S rc INT(10) INZ(0);
         Monitor;

           iconvClose(conv.conv);

         On-error;
           rc = -1;
         Endmon;

         memset(%addr(conv.tocode):0:%size(conv.tocode));
         memset(%addr(conv.fromcode):0:%size(conv.fromcode));
         memset(%addr(conv.conv):0:%size(conv.conv));
 
         return rc;
       end-proc; 


       // *************************************************
       // convert string
       // return (>-1 - good, <0 - error)
       // *************************************************
       dcl-proc convCall;
         dcl-pi *N INT(10);
          conv likeds(ciconv_t);
          buffPtr POINTER;
          buffLen INT(10);
          outPtr POINTER;
          outLen INT(10);
         end-pi;
         DCL-S rc INT(10) INZ(0);
         DCL-S rcb IND INZ(*OFF);
         DCL-S isOpen IND INZ(*OFF);
         DCL-S aConvPtrP POINTER INZ(*NULL);
         DCL-S fromPtr POINTER INZ(*NULL);
         DCL-S toPtr POINTER INZ(*NULL);
         DCL-S aConvSz INT(10) INZ(0);
         DCL-S fromBytes INT(10) INZ(0);
         DCL-S toBytes INT(10) INZ(0);
         DCL-S toMax INT(10) INZ(0);
         DCL-S maxBytes INT(10) INZ(0);
         DCL-S maxOut INT(10) INZ(0);
         DCL-S maxZero INT(10) INZ(0);

         Monitor;

           // size_t iconv (cd, inbuf, inbytesleft, outbuf, outbytesleft)
           // inbytesleft  - number of bytes not converted input buffer
           // outbytesleft - available bytes to end output buffer
           // If an error occurs, iconv() returns -1 
           // in the return value, and sets errno to indicate the error.
           maxBytes   = buffLen;
           fromPtr    = buffPtr;
           fromBytes  = maxBytes;
           toPtr      = outPtr;
           toBytes    = outLen;
           // convert
           toMax = toBytes;
           rc = iconv(conv.conv
                 :%addr(fromPtr):%addr(fromBytes)
                 :%addr(toPtr):%addr(toBytes));
           maxBytes = toMax - toBytes;
           // did it work?
           if rc > -1;
             // out buffer provided (no copy)
             toPtr = outPtr;
             maxOut = outLen;
             // how many bytes unused output buffer?
             outLen = toBytes;
             // possible null terminate
             maxZero = maxOut - maxBytes;
             if maxZero > 4096;
               maxZero = 4096;
             endif;
             if maxZero > 0;
                memset(toPtr + maxBytes:0:maxZero);
             endif;
             // how many input bytes did not convert?
             buffLen = fromBytes;
           endif;

         On-error;
           rc = -1;
         Endmon;
 
         return rc;
       end-proc; 


       // *************************************************
       // convert API -- custom iconv convert
       // return (>-1 - good, <0 - error)
       // *************************************************
       dcl-proc convCCSID export;
         dcl-pi *N INT(10);
          fromCCSID INT(10);
          toCCSID INT(10);
          buffPtr POINTER;
          buffLen INT(10);
          outPtr POINTER;
          outLen INT(10);
         end-pi;
         DCL-S i INT(10) INZ(0);
         DCL-S rc INT(10) INZ(0);
         DCL-S rcb IND INZ(*OFF);
         DCL-DS conv likeds(ciconv_t);

         if fromCCSID < 0;
           fromCCSID = 0;
         endif;
         if toCCSID < 0;
           toCCSID = 0;
         endif;
         if fromCCSID = 0 and toCCSID = 0;
           return 0;
         endif;       
         // scan the cache ...
         for i = 1 to CNVOPNMAX;
           // found in cache?
           select;
           when myiConv(i).conviok = *BLANKS;
             rc = convOpen(fromCCSID:toCCSID:conv);
             memset(%addr(myiConv(i)):0:%size(conv));
             myiConv(i).conviok = CNVOPNOK;
             cpybytes(%addr(myiConv(i)):%addr(conv):%size(conv));
             leave;
           when myiConv(i).fromcode.qtqCCSID = fromCCSID
           and myiConv(i).tocode.qtqCCSID = toCCSID
           and myiConv(i).conviok = CNVOPNOK;
             cpybytes(%addr(conv):%addr(myiConv(i)):%size(conv));
             leave;
           // other?
           other;
           endsl;
         endfor;
         rc = convCall(conv:buffPtr:buffLen:outPtr:outLen);
 
         return rc;
       end-proc; 

       // *************************************************
       // convert API -- custom iconv convert
       // return (>-1 - good, <0 - error)
       // *************************************************
       dcl-proc bigTrim export;
         dcl-pi *N UNS(20);
          start POINTER VALUE;
          len UNS(20) VALUE;
         end-pi;
         DCL-S pCopy POINTER INZ(*NULL);
         DCL-DS myCopy likeds(over_t) based(pCopy);

         dow len > 0;
           pCopy = start + (len-1);
           if myCopy.bytex <> *BLANKS 
           and myCopy.bytex <> x'00';
             leave;
           endif;
           len -= 1;
         enddo;

         return len;
       end-proc; 


       // *************************************************
       // convert API -- custom convert all
       // return (!>-1 - good, <0 - error)
       // *************************************************
       dcl-proc IleInt10PaseInt10 export;
         dcl-pi *N INT(10);
          flag INT(10) VALUE;
          argv POINTER VALUE;
          ilev POINTER VALUE;
          ileDesc likeds(virtVar_t);
          paseDesc likeds(virtVar_t);
         end-pi;
         DCL-S rc INT(10) INZ(-1);
         DCL-S data INT(10) INZ(-1);
         DCL-S datalen INT(10) INZ(-1);
         DCL-S ileOutp POINTER INZ(*NULL);
         DCL-S ileOut POINTER BASED(ileOutp);
         DCL-S paseOutp POINTER INZ(*NULL);
         DCL-S paseOut POINTER BASED(paseOutp);
         DCL-S pCopy POINTER INZ(*NULL);
         DCL-DS myCopy likeds(over_t) based(pCopy);
         DCL-S pCopy1 POINTER INZ(*NULL);
         DCL-DS myCopy1 likeds(over_t) based(pCopy1);


         datalen = %size(data);

         select;
         // pase to ile
         when flag = CNV_IN;
           ileOutp = ilev;
           ileOut = %alloc(datalen);
           memset(ileOut:0:datalen);
           select;
           when ileDesc.iTypeIO = VIRT_IO_OUT;
           when ileDesc.iTypeIO = VIRT_IO_IN
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             pCopy = ileOut;
             pCopy1 = argv;
             myCopy.intx = myCopy1.intx;
           // other?
           other;
           endsl;
         // ile 2 pase
         when flag = CNV_OUT;
           select;
           when ileDesc.iTypeIO = VIRT_IO_IN;
           when ileDesc.iTypeIO = VIRT_IO_OUT
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             // ILE wants to write output
             // size and data of ILE output
             paseOutp = argv;
             paseDesc.iTypeIO = VIRT_IO_OUT;
             paseDesc.iLen = datalen;
             pCopy = paseOut;
             pCopy1 = ilev;
             myCopy.intx = myCopy1.intx;
             paseDesc.paseAddr = 0;
           // other?
           other;
           endsl;
           dealloc(en) ilev;
         // other?
         other;
         endsl;

         return rc;
       end-proc; 

       dcl-proc IleInt10PaseFloat8 export;
         dcl-pi *N INT(10);
          flag INT(10) VALUE;
          argv POINTER VALUE;
          ilev POINTER VALUE;
          ileDesc likeds(virtVar_t);
          paseDesc likeds(virtVar_t);
         end-pi;
         DCL-S rc INT(10) INZ(-1);
         DCL-S data INT(10) INZ(-1);
         DCL-S datalen INT(10) INZ(-1);
         DCL-S data2 FLOAT(8) INZ(-1);
         DCL-S data2len INT(10) INZ(-1);
         DCL-S ileOutp POINTER INZ(*NULL);
         DCL-S ileOut POINTER BASED(ileOutp);
         DCL-S paseOutp POINTER INZ(*NULL);
         DCL-S paseOut POINTER BASED(paseOutp);
         DCL-S pCopy POINTER INZ(*NULL);
         DCL-DS myCopy likeds(over_t) based(pCopy);
         DCL-S pCopy1 POINTER INZ(*NULL);
         DCL-DS myCopy1 likeds(over_t) based(pCopy1);


         datalen = %size(data);
         data2len = %size(data2);

         select;
         // pase to ile
         when flag = CNV_IN;
           ileOutp = ilev;
           ileOut = %alloc(datalen);
           memset(ileOut:0:datalen);
           select;
           when ileDesc.iTypeIO = VIRT_IO_OUT;
           when ileDesc.iTypeIO = VIRT_IO_IN
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             pCopy = ileOut;
             pCopy1 = argv;
             myCopy.intx = myCopy1.double;
           // other?
           other;
           endsl;
         // ile 2 pase
         when flag = CNV_OUT;
           select;
           when ileDesc.iTypeIO = VIRT_IO_IN;
           when ileDesc.iTypeIO = VIRT_IO_OUT
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             // ILE wants to write output
             // size and data of ILE output
             paseOutp = argv;
             paseDesc.iTypeIO = VIRT_IO_OUT;
             paseDesc.iLen = datalen;
             pCopy = paseOut;
             pCopy1 = ilev;
             myCopy.double = myCopy1.intx;
             paseDesc.paseAddr = 0;
           // other?
           other;
           endsl;
           dealloc(en) ilev;
         // other?
         other;
         endsl;

         return rc;
       end-proc; 

       dcl-proc IleFloat8PaseFloat8 export;
         dcl-pi *N INT(10);
          flag INT(10) VALUE;
          argv POINTER VALUE;
          ilev POINTER VALUE;
          ileDesc likeds(virtVar_t);
          paseDesc likeds(virtVar_t);
         end-pi;
         DCL-S rc INT(10) INZ(-1);
         DCL-S data FLOAT(8) INZ(-1);
         DCL-S datalen INT(10) INZ(-1);
         DCL-S ileOutp POINTER INZ(*NULL);
         DCL-S ileOut POINTER BASED(ileOutp);
         DCL-S paseOutp POINTER INZ(*NULL);
         DCL-S paseOut POINTER BASED(paseOutp);
         DCL-S pCopy POINTER INZ(*NULL);
         DCL-DS myCopy likeds(over_t) based(pCopy);
         DCL-S pCopy1 POINTER INZ(*NULL);
         DCL-DS myCopy1 likeds(over_t) based(pCopy1);


         datalen = %size(data);

         select;
         // pase to ile
         when flag = CNV_IN;
           ileOutp = ilev;
           ileOut = %alloc(datalen);
           memset(ileOut:0:datalen);
           select;
           when ileDesc.iTypeIO = VIRT_IO_OUT;
           when ileDesc.iTypeIO = VIRT_IO_IN
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             pCopy = ileOut;
             pCopy1 = argv;
             myCopy.double = myCopy1.double;
           // other?
           other;
           endsl;
         // ile 2 pase
         when flag = CNV_OUT;
           select;
           when ileDesc.iTypeIO = VIRT_IO_IN;
           when ileDesc.iTypeIO = VIRT_IO_OUT
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             // ILE wants to write output
             // size and data of ILE output
             paseOutp = argv;
             paseDesc.iTypeIO = VIRT_IO_OUT;
             paseDesc.iLen = datalen;
             pCopy = paseOut;
             pCopy1 = ilev;
             myCopy.double = myCopy1.double;
             paseDesc.paseAddr = 0;
           // other?
           other;
           endsl;
           dealloc(en) ilev;
         // other?
         other;
         endsl;

         return rc;
       end-proc; 


       dcl-proc IleFloat8PaseInt10 export;
         dcl-pi *N INT(10);
          flag INT(10) VALUE;
          argv POINTER VALUE;
          ilev POINTER VALUE;
          ileDesc likeds(virtVar_t);
          paseDesc likeds(virtVar_t);
         end-pi;
         DCL-S rc INT(10) INZ(-1);
         DCL-S data FLOAT(8) INZ(-1);
         DCL-S datalen INT(10) INZ(-1);
         DCL-S data2 INT(10) INZ(-1);
         DCL-S data2len INT(10) INZ(-1);
         DCL-S ileOutp POINTER INZ(*NULL);
         DCL-S ileOut POINTER BASED(ileOutp);
         DCL-S paseOutp POINTER INZ(*NULL);
         DCL-S paseOut POINTER BASED(paseOutp);
         DCL-S pCopy POINTER INZ(*NULL);
         DCL-DS myCopy likeds(over_t) based(pCopy);
         DCL-S pCopy1 POINTER INZ(*NULL);
         DCL-DS myCopy1 likeds(over_t) based(pCopy1);


         datalen = %size(data);
         data2len = %size(data2);

         select;
         // pase to ile
         when flag = CNV_IN;
           ileOutp = ilev;
           ileOut = %alloc(datalen);
           memset(ileOut:0:datalen);
           select;
           when ileDesc.iTypeIO = VIRT_IO_OUT;
           when ileDesc.iTypeIO = VIRT_IO_IN
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             pCopy = ileOut;
             pCopy1 = argv;
             myCopy.double = myCopy1.intx;
           // other?
           other;
           endsl;
         // ile 2 pase
         when flag = CNV_OUT;
           select;
           when ileDesc.iTypeIO = VIRT_IO_IN;
           when ileDesc.iTypeIO = VIRT_IO_OUT
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             // ILE wants to write output
             // size and data of ILE output
             paseOutp = argv;
             paseDesc.iTypeIO = VIRT_IO_OUT;
             paseDesc.iLen = data2len;
             pCopy = paseOut;
             pCopy1 = ilev;
             myCopy.intx = myCopy1.double;
             paseDesc.paseAddr = 0;
           // other?
           other;
           endsl;
           dealloc(en) ilev;
         // other?
         other;
         endsl;

         return rc;
       end-proc; 


       dcl-proc IlePackedPaseFloat8 export;
         dcl-pi *N INT(10);
          flag INT(10) VALUE;
          argv POINTER VALUE;
          ilev POINTER VALUE;
          ileDesc likeds(virtVar_t);
          paseDesc likeds(virtVar_t);
         end-pi;
         DCL-S rc INT(10) INZ(-1);
         DCL-S data PACKED(15:4) INZ(-1);
         DCL-S datalen INT(10) INZ(-1);
         DCL-S data2 FLOAT(8) INZ(-1);
         DCL-S data2len INT(10) INZ(-1);
         DCL-S ileOutp POINTER INZ(*NULL);
         DCL-S ileOut POINTER BASED(ileOutp);
         DCL-S paseOutp POINTER INZ(*NULL);
         DCL-S paseOut POINTER BASED(paseOutp);
         DCL-S pCopy POINTER INZ(*NULL);
         DCL-DS myCopy likeds(over_t) based(pCopy);
         DCL-S pCopy1 POINTER INZ(*NULL);
         DCL-DS myCopy1 likeds(over_t) based(pCopy1);
         DCL-S l INT(10) INZ(-1);
         DCL-S declen INT(10) INZ(-1);

         // calculate size of packed
         l = ileDesc.iLen;
         if %REM(l:2)=0;
           l += 1;
         endif;
         select;
         when l = 3;
           declen = 2;
         when l = 2;
           declen = 2;
         when l = 1;
           declen = 1;
         other;
           declen=%DIV((l+1):2); 
         endsl;
         datalen = declen;
         data2len = %size(data2);

         select;
         // pase to ile
         when flag = CNV_IN;
           ileOutp = ilev;
           ileOut = %alloc(datalen);
           memset(ileOut:0:datalen);
           select;
           when ileDesc.iTypeIO = VIRT_IO_OUT;
           when ileDesc.iTypeIO = VIRT_IO_IN
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             pCopy = ileOut;
             pCopy1 = argv;
             QXXDTOP(pCopy:ileDesc.iLen:ileDesc.iScale:myCopy1.double);
           // other?
           other;
           endsl;
         // ile 2 pase
         when flag = CNV_OUT;
           select;
           when ileDesc.iTypeIO = VIRT_IO_IN;
           when ileDesc.iTypeIO = VIRT_IO_OUT
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             // ILE wants to write output
             // size and data of ILE output
             paseOutp = argv;
             paseDesc.iTypeIO = VIRT_IO_OUT;
             paseDesc.iLen = datalen;
             pCopy = paseOut;
             pCopy1 = ilev;
             myCopy.double = QXXPTOD(pCopy1:ileDesc.iLen:ileDesc.iScale);
             paseDesc.paseAddr = 0;
           // other?
           other;
           endsl;
           dealloc(en) ilev;
         // other?
         other;
         endsl;

         return rc;
       end-proc; 

       dcl-proc IlePackedPaseInt10 export;
         dcl-pi *N INT(10);
          flag INT(10) VALUE;
          argv POINTER VALUE;
          ilev POINTER VALUE;
          ileDesc likeds(virtVar_t);
          paseDesc likeds(virtVar_t);
         end-pi;
         DCL-S rc INT(10) INZ(-1);
         DCL-S data PACKED(15:4) INZ(-1);
         DCL-S datalen INT(10) INZ(-1);
         DCL-S data2 FLOAT(8) INZ(-1);
         DCL-S data2len INT(10) INZ(-1);
         DCL-S ileOutp POINTER INZ(*NULL);
         DCL-S ileOut POINTER BASED(ileOutp);
         DCL-S paseOutp POINTER INZ(*NULL);
         DCL-S paseOut POINTER BASED(paseOutp);
         DCL-S pCopy POINTER INZ(*NULL);
         DCL-DS myCopy likeds(over_t) based(pCopy);
         DCL-S pCopy1 POINTER INZ(*NULL);
         DCL-DS myCopy1 likeds(over_t) based(pCopy1);
         DCL-S l INT(10) INZ(-1);
         DCL-S declen INT(10) INZ(-1);

         // calculate size of packed
         l = ileDesc.iLen;
         if %REM(l:2)=0;
           l += 1;
         endif;
         select;
         when l = 3;
           declen = 2;
         when l = 2;
           declen = 2;
         when l = 1;
           declen = 1;
         other;
           declen=%DIV((l+1):2); 
         endsl;
         datalen = declen;
         data2len = %size(data2);

         select;
         // pase to ile
         when flag = CNV_IN;
           ileOutp = ilev;
           ileOut = %alloc(datalen);
           memset(ileOut:0:datalen);
           select;
           when ileDesc.iTypeIO = VIRT_IO_OUT;
           when ileDesc.iTypeIO = VIRT_IO_IN
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             pCopy = ileOut;
             pCopy1 = argv;
             data2 = myCopy1.double;
             QXXDTOP(pCopy:ileDesc.iLen:ileDesc.iScale:data2);
           // other?
           other;
           endsl;
         // ile 2 pase
         when flag = CNV_OUT;
           select;
           when ileDesc.iTypeIO = VIRT_IO_IN;
           when ileDesc.iTypeIO = VIRT_IO_OUT
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             // ILE wants to write output
             // size and data of ILE output
             paseOutp = argv;
             paseDesc.iTypeIO = VIRT_IO_OUT;
             paseDesc.iLen = datalen;
             pCopy = paseOut;
             pCopy1 = ilev;
             myCopy.intx = QXXPTOD(pCopy1:ileDesc.iLen:ileDesc.iScale);
             paseDesc.paseAddr = 0;
           // other?
           other;
           endsl;
           dealloc(en) ilev;
         // other?
         other;
         endsl;

         return rc;
       end-proc; 


       dcl-proc IleZonedPaseFloat8 export;
         dcl-pi *N INT(10);
          flag INT(10) VALUE;
          argv POINTER VALUE;
          ilev POINTER VALUE;
          ileDesc likeds(virtVar_t);
          paseDesc likeds(virtVar_t);
         end-pi;
         DCL-S rc INT(10) INZ(-1);
         DCL-S data ZONED(7:4) INZ(-1);
         DCL-S datalen INT(10) INZ(-1);
         DCL-S data2 FLOAT(8) INZ(-1);
         DCL-S data2len INT(10) INZ(-1);
         DCL-S ileOutp POINTER INZ(*NULL);
         DCL-S ileOut POINTER BASED(ileOutp);
         DCL-S paseOutp POINTER INZ(*NULL);
         DCL-S paseOut POINTER BASED(paseOutp);
         DCL-S pCopy POINTER INZ(*NULL);
         DCL-DS myCopy likeds(over_t) based(pCopy);
         DCL-S pCopy1 POINTER INZ(*NULL);
         DCL-DS myCopy1 likeds(over_t) based(pCopy1);

         datalen = ileDesc.iLen;
         data2len = %size(data2);

         select;
         // pase to ile
         when flag = CNV_IN;
           ileOutp = ilev;
           ileOut = %alloc(datalen);
           memset(ileOut:0:datalen);
           select;
           when ileDesc.iTypeIO = VIRT_IO_OUT;
           when ileDesc.iTypeIO = VIRT_IO_IN
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             pCopy = ileOut;
             pCopy1 = argv;
             QXXDTOZ(pCopy:ileDesc.iLen:ileDesc.iScale:myCopy1.double);
           // other?
           other;
           endsl;
         // ile 2 pase
         when flag = CNV_OUT;
           select;
           when ileDesc.iTypeIO = VIRT_IO_IN;
           when ileDesc.iTypeIO = VIRT_IO_OUT
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             // ILE wants to write output
             // size and data of ILE output
             paseOutp = argv;
             paseDesc.iTypeIO = VIRT_IO_OUT;
             paseDesc.iLen = datalen;
             pCopy = paseOut;
             pCopy1 = ilev;
             myCopy.double = QXXZTOD(pCopy1:ileDesc.iLen:ileDesc.iScale);
             paseDesc.paseAddr = 0;
           // other?
           other;
           endsl;
           dealloc(en) ilev;
         // other?
         other;
         endsl;

         return rc;
       end-proc; 


       dcl-proc IleZonedPaseInt10 export;
         dcl-pi *N INT(10);
          flag INT(10) VALUE;
          argv POINTER VALUE;
          ilev POINTER VALUE;
          ileDesc likeds(virtVar_t);
          paseDesc likeds(virtVar_t);
         end-pi;
         DCL-S rc INT(10) INZ(-1);
         DCL-S data ZONED(7:4) INZ(-1);
         DCL-S datalen INT(10) INZ(-1);
         DCL-S data2 FLOAT(8) INZ(-1);
         DCL-S data2len INT(10) INZ(-1);
         DCL-S ileOutp POINTER INZ(*NULL);
         DCL-S ileOut POINTER BASED(ileOutp);
         DCL-S paseOutp POINTER INZ(*NULL);
         DCL-S paseOut POINTER BASED(paseOutp);
         DCL-S pCopy POINTER INZ(*NULL);
         DCL-DS myCopy likeds(over_t) based(pCopy);
         DCL-S pCopy1 POINTER INZ(*NULL);
         DCL-DS myCopy1 likeds(over_t) based(pCopy1);

         datalen = ileDesc.iLen;
         data2len = %size(data2);

         select;
         // pase to ile
         when flag = CNV_IN;
           ileOutp = ilev;
           ileOut = %alloc(datalen);
           memset(ileOut:0:datalen);
           select;
           when ileDesc.iTypeIO = VIRT_IO_OUT;
           when ileDesc.iTypeIO = VIRT_IO_IN
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             pCopy = ileOut;
             pCopy1 = argv;
             data2 = myCopy1.intx;
             QXXDTOZ(pCopy:ileDesc.iLen:ileDesc.iScale:data2);
           // other?
           other;
           endsl;
         // ile 2 pase
         when flag = CNV_OUT;
           select;
           when ileDesc.iTypeIO = VIRT_IO_IN;
           when ileDesc.iTypeIO = VIRT_IO_OUT
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             // ILE wants to write output
             // size and data of ILE output
             paseOutp = argv;
             paseDesc.iTypeIO = VIRT_IO_OUT;
             paseDesc.iLen = datalen;
             pCopy = paseOut;
             pCopy1 = ilev;
             myCopy.intx = QXXZTOD(pCopy1:ileDesc.iLen:ileDesc.iScale);
             paseDesc.paseAddr = 0;
           // other?
           other;
           endsl;
           dealloc(en) ilev;
         // other?
         other;
         endsl;

         return rc;
       end-proc; 


       dcl-proc IleCharPaseString export;
         dcl-pi *N INT(10);
          flag INT(10) VALUE;
          argv POINTER VALUE;
          ilev POINTER VALUE;
          ileDesc likeds(virtVar_t);
          paseDesc likeds(virtVar_t);
          ileCCSID INT(10);
          paseCCSID INT(10);
         end-pi;
         DCL-S i INT(10) INZ(0);
         DCL-S rc INT(10) INZ(-1);
         DCL-S rcb IND INZ(*OFF);
         DCL-S ileOutp POINTER INZ(*NULL);
         DCL-S ileOut POINTER BASED(ileOutp);
         DCL-S paseOutp POINTER INZ(*NULL);
         DCL-S paseOut POINTER BASED(paseOutp);
         DCL-S buffLen INT(10) INZ(0);
         DCL-S outLen INT(10) INZ(0);
         DCL-S buffPtr POINTER INZ(*NULL);
         DCL-S outPtr POINTER INZ(*NULL);
         DCL-S pmem_pase UNS(20) INZ(0);
         DCL-S len INT(10) INZ(0);

         select;
         // pase to ile
         when flag = CNV_IN;
           ileOutp = ilev;
           ileOut = %alloc(ileDesc.iLen + 1);
           memset(ileOut:0:ileDesc.iLen + 1);
           select;
           when ileDesc.iTypeIO = VIRT_IO_OUT;
           when ileDesc.iTypeIO = VIRT_IO_IN
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             buffPtr = argv;
             buffLen = paseDesc.iLen + 1;
             outPtr = ileOut;
             outLen = ileDesc.iLen + 1;
             rc = convCCSID(paseCCSID:ileCCSID:buffPtr:buffLen:outPtr:outLen);
           // other?
           other;
           endsl;
         // ile 2 pase
         when flag = CNV_OUT;
           select;
           when ileDesc.iTypeIO = VIRT_IO_IN;
           when ileDesc.iTypeIO = VIRT_IO_OUT
             or ileDesc.iTypeIO = VIRT_IO_BOTH;
             len = bigTrim(ilev:ileDesc.iLen + 1);
             paseOutp = argv;
             paseOut = Qp2Malloc(len + 1:%addr(pmem_pase));
             memset(paseOut:0:len + 1);
             buffPtr = ilev;
             buffLen = len + 1;
             outPtr = paseOut;
             outLen = len + 1;
             rc = convCCSID(ileCCSID:paseCCSID:buffPtr:buffLen:outPtr:outLen);
             // ILE wants to write output
             // size and data of ILE output
             paseDesc.iTypeIO = VIRT_IO_OUT;
             paseDesc.iLen = len;
             paseDesc.paseAddr = pmem_pase;
           // other?
           other;
           endsl;
           dealloc(en) ilev;
         // other?
         other;
         endsl;


         return rc;
       end-proc; 


       // *************************************************
       // convert API -- custom convert all
       // return (!>-1 - good, <0 - error)
       // *************************************************
       dcl-proc convToILE export;
         dcl-pi *N INT(10);
          argv POINTER VALUE;
          ilev POINTER VALUE;
          ileDesc likeds(virtVar_t);
          paseDesc likeds(virtVar_t);
          ileCCSID INT(10);
          paseCCSID INT(10);
         end-pi;
         DCL-S i INT(10) INZ(0);
         DCL-S rc INT(10) INZ(-1);
         DCL-S rcb IND INZ(*OFF);
         DCL-DS conv likeds(ciconv_t);

         // copy in (converts to ile)
         select;
         when ileDesc.iType = VIRT_CHAR;
           select;
           when paseDesc.iType = VIRT_STRING;
             rc = IleCharPaseString(CNV_IN:argv:ilev
                  :ileDesc:paseDesc:ileCCSID:paseCCSID);
           when paseDesc.iType = VIRT_LONG;
           when paseDesc.iType = VIRT_DOUBLE;
           // other?
           other;
           endsl;
         when ileDesc.iType = VIRT_VARCHAR;
         when ileDesc.iType = VIRT_USC2;
         when ileDesc.iType = VIRT_VARUSC2;
         when ileDesc.iType = VIRT_GRAPH;
         when ileDesc.iType = VIRT_VARGRAPH;
         when ileDesc.iType = VIRT_IND;
         when ileDesc.iType = VIRT_PACKED;
           select;
           when paseDesc.iType = VIRT_STRING;
           when paseDesc.iType = VIRT_LONG;
             rc = IlePackedPaseInt10(CNV_IN:argv:ilev
                  :ileDesc:paseDesc);
           when paseDesc.iType = VIRT_DOUBLE;
             rc = IlePackedPaseFloat8(CNV_IN:argv:ilev
                  :ileDesc:paseDesc);
           // other?
           other;
           endsl;
         when ileDesc.iType = VIRT_ZONED;
           select;
           when paseDesc.iType = VIRT_STRING;
           when paseDesc.iType = VIRT_LONG;
             rc = IleZonedPaseInt10(CNV_IN:argv:ilev
                  :ileDesc:paseDesc);
           when paseDesc.iType = VIRT_DOUBLE;
             rc = IleZonedPaseFloat8(CNV_IN:argv:ilev
                  :ileDesc:paseDesc);
           // other?
           other;
           endsl;
         when ileDesc.iType = VIRT_BINDEC;
         when ileDesc.iType = VIRT_INT3;
         when ileDesc.iType = VIRT_INT5;
         when ileDesc.iType = VIRT_INT10;
           select;
           when paseDesc.iType = VIRT_STRING;
           when paseDesc.iType = VIRT_LONG;
             rc = ILEInt10PaseInt10(CNV_IN:argv:ilev
                  :ileDesc:paseDesc);
           when paseDesc.iType = VIRT_DOUBLE;
             rc = ILEInt10PaseFloat8(CNV_IN:argv:ilev
                  :ileDesc:paseDesc);
           // other?
           other;
           endsl;
         when ileDesc.iType = VIRT_IN20;
         when ileDesc.iType = VIRT_UNS3;
         when ileDesc.iType = VIRT_UNS5;
         when ileDesc.iType = VIRT_UNS10;
         when ileDesc.iType = VIRT_UNS20;
         when ileDesc.iType = VIRT_FLOAT4;
         when ileDesc.iType = VIRT_FLOAT8;
           select;
           when paseDesc.iType = VIRT_STRING;
           when paseDesc.iType = VIRT_LONG;
             rc = IleFloat8PaseInt10(CNV_IN:argv:ilev
                  :ileDesc:paseDesc);
           when paseDesc.iType = VIRT_DOUBLE;
             rc = IleFloat8PaseFloat8(CNV_IN:argv:ilev
                  :ileDesc:paseDesc);
           // other?
           other;
           endsl;
         when ileDesc.iType = VIRT_DATE;
         when ileDesc.iType = VIRT_TIME;
         when ileDesc.iType = VIRT_TIMESTAMP;
         when ileDesc.iType = VIRT_POINTER;
         when ileDesc.iType = VIRT_POINTERPROC;
         when ileDesc.iType = VIRT_STRING;
         // other?
         other;
         endsl;
 
         return rc;
       end-proc; 


       // *************************************************
       // convert API -- custom convert all
       // return (!>-1 - good, <0 - error)
       // *************************************************
       dcl-proc convToPase export;
         dcl-pi *N INT(10);
          argv POINTER VALUE;
          ilev POINTER VALUE;
          ileDesc likeds(virtVar_t);
          paseDesc likeds(virtVar_t);
          ileCCSID INT(10);
          paseCCSID INT(10);
         end-pi;
         DCL-S i INT(10) INZ(0);
         DCL-S rc INT(10) INZ(-1);
         DCL-S rcb IND INZ(*OFF);
         DCL-DS conv likeds(ciconv_t);

         // copy in (converts from ile)
         select;
         when ileDesc.iType = VIRT_CHAR;
           select;
           when paseDesc.iType = VIRT_STRING;
             rc = IleCharPaseString(CNV_OUT:argv:ilev
                  :ileDesc:paseDesc:ileCCSID:paseCCSID);
           when paseDesc.iType = VIRT_LONG;  
           when paseDesc.iType = VIRT_DOUBLE;
           // other?
           other;
           endsl;
         when ileDesc.iType = VIRT_VARCHAR;
         when ileDesc.iType = VIRT_USC2;
         when ileDesc.iType = VIRT_VARUSC2;
         when ileDesc.iType = VIRT_GRAPH;
         when ileDesc.iType = VIRT_VARGRAPH;
         when ileDesc.iType = VIRT_IND;
         when ileDesc.iType = VIRT_PACKED;
           select;
           when paseDesc.iType = VIRT_STRING;
           when paseDesc.iType = VIRT_LONG;
             rc = IlePackedPaseInt10(CNV_OUT:argv:ilev
                  :ileDesc:paseDesc);
           when paseDesc.iType = VIRT_DOUBLE;
             rc = IlePackedPaseFloat8(CNV_OUT:argv:ilev
                  :ileDesc:paseDesc);
           // other?
           other;
           endsl;
         when ileDesc.iType = VIRT_ZONED;
           select;
           when paseDesc.iType = VIRT_STRING;
           when paseDesc.iType = VIRT_LONG;
             rc = IleZonedPaseInt10(CNV_OUT:argv:ilev
                  :ileDesc:paseDesc);
           when paseDesc.iType = VIRT_DOUBLE;
             rc = IleZonedPaseFloat8(CNV_OUT:argv:ilev
                  :ileDesc:paseDesc);
           // other?
           other;
           endsl;
         when ileDesc.iType = VIRT_BINDEC;
         when ileDesc.iType = VIRT_INT3;
         when ileDesc.iType = VIRT_INT5;
         when ileDesc.iType = VIRT_INT10;
           select;
           when paseDesc.iType = VIRT_STRING;
           when paseDesc.iType = VIRT_LONG;
             rc = ILEInt10PaseInt10(CNV_OUT:argv:ilev
                  :ileDesc:paseDesc);
           when paseDesc.iType = VIRT_DOUBLE;
             rc = ILEInt10PaseFloat8(CNV_OUT:argv:ilev
                  :ileDesc:paseDesc);
           // other?
           other;
           endsl;
         when ileDesc.iType = VIRT_IN20;
         when ileDesc.iType = VIRT_UNS3;
         when ileDesc.iType = VIRT_UNS5;
         when ileDesc.iType = VIRT_UNS10;
         when ileDesc.iType = VIRT_UNS20;
         when ileDesc.iType = VIRT_FLOAT4;
         when ileDesc.iType = VIRT_FLOAT8;
           select;
           when paseDesc.iType = VIRT_STRING;
           when paseDesc.iType = VIRT_LONG;
             rc = IleFloat8PaseInt10(CNV_OUT:argv:ilev
                  :ileDesc:paseDesc);
           when paseDesc.iType = VIRT_DOUBLE;
             rc = IleFloat8PaseFloat8(CNV_OUT:argv:ilev
                  :ileDesc:paseDesc);
           // other?
           other;
           endsl;
         when ileDesc.iType = VIRT_DATE;
         when ileDesc.iType = VIRT_TIME;
         when ileDesc.iType = VIRT_TIMESTAMP;
         when ileDesc.iType = VIRT_POINTER;
         when ileDesc.iType = VIRT_POINTERPROC;
         when ileDesc.iType = VIRT_STRING;
         // other?
         other;
         endsl;
 
         return rc;
       end-proc; 





