      /if defined(VIRT_H)
      /eof
      /endif
      /define VIRT_H

       // *************************************************
       // virtual table -- max allowed
       // *************************************************
       DCL-C VIRT_TABLE_MAX CONST(4096);
       DCL-C VIRT_CALLARG_MAX CONST(32);

       // *************************************************
       // virtual table -- ILE types
       // *************************************************
       DCL-C VIRT_CHAR CONST(1);
       DCL-C VIRT_VARCHAR CONST(2);
       DCL-C VIRT_USC2 CONST(3);
       DCL-C VIRT_VARUSC2 CONST(4);
       DCL-C VIRT_GRAPH CONST(5);
       DCL-C VIRT_VARGRAPH CONST(6);
       DCL-C VIRT_IND CONST(7);
       DCL-C VIRT_PACKED CONST(8);
       DCL-C VIRT_ZONED CONST(9);
       DCL-C VIRT_BINDEC CONST(10);
       DCL-C VIRT_INT3 CONST(11);
       DCL-C VIRT_INT5 CONST(12);
       DCL-C VIRT_INT10 CONST(13);
       DCL-C VIRT_LONG CONST(13);
       DCL-C VIRT_IN20 CONST(14);
       DCL-C VIRT_UNS3 CONST(15);
       DCL-C VIRT_UNS5 CONST(16);
       DCL-C VIRT_UNS10 CONST(17);
       DCL-C VIRT_UNS20 CONST(18);
       DCL-C VIRT_FLOAT4 CONST(19);
       DCL-C VIRT_FLOAT8 CONST(20);
       DCL-C VIRT_DOUBLE CONST(20);
       DCL-C VIRT_DATE CONST(21);
       DCL-C VIRT_TIME CONST(22);
       DCL-C VIRT_TIMESTAMP CONST(23);
       DCL-C VIRT_POINTER CONST(24);
       DCL-C VIRT_POINTERPROC CONST(25);
       DCL-C VIRT_STRING CONST(26);

       DCL-C VIRT_IO_IN CONST(1);
       DCL-C VIRT_IO_OUT CONST(2);
       DCL-C VIRT_IO_BOTH CONST(3);

       // *************************************************
       // virtual table API -- templates
       // *************************************************
       dcl-ds virtVar_t qualified template; 
         iType INT(10); 
         iLen INT(10); 
         iScale INT(10);
         iTypeIO INT(10);
         paseAddr UNS(20);
       end-ds;

       dcl-ds virtTable_t qualified template; 
         iCall POINTER(*PROC); 
         iName POINTER;
         iNameLen INT(10);
         iVar likeds(virtVar_t) dim(VIRT_CALLARG_MAX); 
       end-ds;

       // *************************************************
       // virtual table API -- add your ILE call to table
       // *************************************************
       dcl-pr VirtualAddCallBack IND;
         iName CHAR(1024) VALUE;
         iCall POINTER(*PROC) VALUE;
       end-pr;

       // *************************************************
       // virtual table API -- add param description
       // *************************************************
       dcl-pr VirtualAddCallBackDesc IND;
         iName CHAR(1024) VALUE;
         iType INT(10) VALUE; 
         iLen INT(10) VALUE; 
         iScale INT(10) VALUE;
         iTypeIO INT(10) VALUE;
       end-pr;

       // *************************************************
       // virtual table API -- call PASE script
       // *************************************************
       dcl-pr  VirtualCallScript32 IND;
         scriptChunk CHAR(65500) VALUE;
       end-pr;

       // *************************************************
       // virtual table API -- language (vphp, ...)
       // *************************************************
       dcl-pr VirtualSetLanguage;
         iCopy POINTER(*PROC) VALUE;
       end-pr;


