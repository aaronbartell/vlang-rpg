#!/bin/sh
RPGLIB="VLANG"
RPGFILES='vconv virt vpase vphp'
system "CRTLIB LIB($RPGLIB) TYPE(*PROD) TEXT('RPG call scripts')"
for i in $RPGFILES ; do
  echo '===================================='
  echo "==> $RPGLIB/$i ..."
  RES=$(system "CRTRPGMOD MODULE($RPGLIB/$i) SRCSTMF('$i.rpgle') DBGVIEW(*SOURCE) OUTPUT(*PRINT) REPLACE(*YES)")
  OK=$(echo "$RES" | grep -c "00 highest severity")
  if (($OK==0))
  then
    echo "$RES"
    echo "==> $RPGLIB/$i -- $OK of 00 highest severity"
    BAD=$(echo "$RES" | grep "*RNF")
    echo '===================================='
    echo '===================================='
    echo "$BAD"
    echo '===================================='
    echo '===================================='
    exit
  else
    echo "==> $RPGLIB/$i -- 00 highest severity"
  fi
  echo '===================================='
done
RPGSRVPGM="CRTSRVPGM SRVPGM($RPGLIB/VPHP) MODULE("
for i in $RPGFILES ; do
  RPGSRVPGM="$RPGSRVPGM $RPGLIB/$i"
done
RPGSRVPGM="$RPGSRVPGM) EXPORT(*ALL) ACTGRP(*CALLER)"
echo "$RPGSRVPGM\n";
system "$RPGSRVPGM"

