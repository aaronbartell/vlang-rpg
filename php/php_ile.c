/*
       // *****************************************************
       // * Copyright (c) 2016, IBM Corporation
       // * All rights reserved.
       // *
       // * Redistribution and use in source and binary forms,
       // * with or without modification, are permitted provided
       // * that the following conditions are met:
       // * - Redistributions of source code must retain
       // *   the above copyright notice, this list of conditions
       // *   and the following disclaimer.
       // * - Redistributions in binary form must reproduce the
       // *   above copyright notice, this list of conditions
       // *   and the following disclaimer in the documentation
       // *   and/or other materials provided with the distribution.
       // * - Neither the name of the IBM Corporation nor the names
       // *   of its contributors may be used to endorse or promote
       // *   products derived from this software without specific
       // *   prior written permission.
       // *
       // * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
       // * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
       // * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
       // * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
       // * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
       // * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
       // * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
       // * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
       // * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
       // * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
       // * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
       // * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
       // * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
       // * POSSIBILITY OF SUCH DAMAGE.
       // *****************************************************

  $Id$
*/


#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "php.h"
#include "php_globals.h"
#include "ext/standard/info.h"
#include "ext/standard/php_string.h"
#include "ext/standard/basic_functions.h"

#include "php_ini.h"
#include "php_ile.h"
#include <as400_protos.h>
#include <as400_types.h>

/* True globals, no need for thread safety */
static int le_result, le_link, le_plink;

ZEND_DECLARE_MODULE_GLOBALS(ile)

static void _free_ile_result(zend_rsrc_list_entry *rsrc TSRMLS_DC)
{
}
static void _close_ile_link(zend_rsrc_list_entry *rsrc TSRMLS_DC)
{
}
static void _close_ile_plink(zend_rsrc_list_entry *rsrc TSRMLS_DC)
{
}
static void _init_ile_globals(zend_ile_globals *ile_globals)
{
	ile_globals->i5_allow_commit = -42;		/* orig  - IBM i legacy CRTLIB containers fail under commit control (isolation *NONE) */
}

/* {{{ ile_functions[]
 */
static const zend_function_entry ile_functions[] = {
	PHP_FE(ile_connect, NULL)
	PHP_FE(ile_callback, NULL)
	PHP_FE_END
};
/* }}} */

/* {{{ ile_module_entry
 */
zend_module_entry ile_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
	STANDARD_MODULE_HEADER,
#endif
	"ile",
	ile_functions,
	PHP_MINIT(ile),
	PHP_MSHUTDOWN(ile),
	NULL,		/* Replace with NULL if there's nothing to do at request start */
	PHP_RSHUTDOWN(ile),
	PHP_MINFO(ile),
#if ZEND_MODULE_API_NO >= 20010901
	PHP_ILE_VERSION, /* Replace with version number for your extension */
#endif
	STANDARD_MODULE_PROPERTIES
};
/* }}} */


#ifdef COMPILE_DL_ILE
ZEND_GET_MODULE(ile)
#endif


/* {{{ PHP_INI */
PHP_INI_BEGIN()
	STD_PHP_INI_BOOLEAN("ile.i5_allow_commit", "-42", PHP_INI_SYSTEM, OnUpdateLong, i5_allow_commit, zend_ile_globals, ile_globals)
PHP_INI_END()
/* }}} */


/* {{{ PHP_MINIT_FUNCTION
 */
PHP_MINIT_FUNCTION(ile)
{
	ZEND_INIT_MODULE_GLOBALS(ile, _init_ile_globals, NULL);

	REGISTER_INI_ENTRIES();
	le_result = zend_register_list_destructors_ex(_free_ile_result, NULL, "ile result", module_number);
	le_link = zend_register_list_destructors_ex(_close_ile_link, NULL, "ile link", module_number);
	le_plink = zend_register_list_destructors_ex(NULL, _close_ile_plink, "ile link persistent", module_number);

	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MSHUTDOWN_FUNCTION
 */
PHP_MSHUTDOWN_FUNCTION(ile)
{
	UNREGISTER_INI_ENTRIES();
	return SUCCESS;
}
/* }}} */


/* {{{ PHP_RSHUTDOWN_FUNCTION
 */
PHP_RSHUTDOWN_FUNCTION(ile)
{
	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION
 */
PHP_MINFO_FUNCTION(ile)
{
	php_info_print_table_start();
	php_info_print_table_header(2, "ILE support", "enabled");
	php_info_print_table_end();

	DISPLAY_INI_ENTRIES();

}
/* }}} */

#define ROUND_QUAD(x) (((size_t)(x) + 0xf) & ~0xf)
#define VIRT_CALLARG_MAX 32

void _ile_call_script(char * code) {
	zend_eval_string(code, NULL, (char *)"" TSRMLS_CC);
}

int vlangActMark;
static int _VirtualLang() {
	int rc = 0;
	if (!vlangActMark) {
		vlangActMark = _ILELOAD("VLANG/VPHP", ILELOAD_LIBOBJ);
	}
	if (vlangActMark < 1) {
		return 0;
	}
	return 1;
}

typedef struct VirtualInitStruct {ILEarglist_base base; ILEpointer p; } VirtualInitStruct;
static char VirtualInitBuf[256];
static char * VirtualInitPtr;
static int _VirtualInit() {
	int rc = 0;
	VirtualInitStruct * arglist = (VirtualInitStruct *) NULL;
	char buffer[ sizeof(VirtualInitStruct) + 16 ];
	static arg_type_t VirtualInitSigStruct[] = { ARG_MEMPTR, ARG_END };
	rc = _VirtualLang();
	if (rc != 1) {
		return 0;
	}
	if (!VirtualInitPtr) {
		VirtualInitPtr = (char *)ROUND_QUAD(VirtualInitBuf);
		rc = _ILESYM((ILEpointer *)VirtualInitPtr, vlangActMark, "VIRTUALINIT");
		if (rc < 0) {
			return 0;
		}
		memset(buffer,0,sizeof(buffer));
		arglist = (VirtualInitStruct *)ROUND_QUAD(buffer);
		arglist->p.s.addr = (address64_t) _ile_call_script;
		rc = _ILECALL((ILEpointer *)VirtualInitPtr, &arglist->base, VirtualInitSigStruct, RESULT_INT32);
		if (rc != ILECALL_NOERROR) {
			return 0;
		}
	}
	return 1;
}

typedef struct virtVar_t {int iType; int iLen; int iScale; int iTypeIO; address64_t paseAddr; } virtVar_t;
typedef struct VirtualCallBack32Struct {ILEarglist_base base; ILEpointer n; ILEpointer d; ILEpointer p[VIRT_CALLARG_MAX]; } VirtualCallBack32Struct;
static char VirtualCallBack32Buf[256];
static char * VirtualCallBack32Ptr;
static int _VirtualCallBack32(char * name, virtVar_t *desc, int argc, char *argv[]) {
	int rc = 0, i = 0, idx = -1;
	VirtualCallBack32Struct * arglist = (VirtualCallBack32Struct *) NULL;
	char buffer[ sizeof(VirtualCallBack32Struct) + 16 ];
	static arg_type_t VirtualCallBack32SigStruct[] = { ARG_MEMPTR, ARG_MEMPTR,
	ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,
	ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,
	ARG_MEMPTR,ARG_MEMPTR,
	ARG_END 
	};
	rc = _VirtualLang();
	if (rc != 1) {
		return 0;
	}
	if (!VirtualCallBack32Ptr) {
		VirtualCallBack32Ptr = (char *)ROUND_QUAD(VirtualCallBack32Buf);
		rc = _ILESYM((ILEpointer *)VirtualCallBack32Ptr, vlangActMark, "VIRTUALCALLBACK32");
		if (rc < 0) {
			return 0;
		}
	}
	memset(buffer,0,sizeof(buffer));
	arglist = (VirtualCallBack32Struct *)ROUND_QUAD(buffer);
	arglist->n.s.addr = (address64_t) name;
	arglist->d.s.addr = (address64_t) desc;
	for (i=0;i<argc;i++) {
		arglist->p[i].s.addr = (address64_t) argv[i];
	}
	rc = _ILECALL((ILEpointer *)VirtualCallBack32Ptr, &arglist->base, VirtualCallBack32SigStruct, RESULT_INT32);
	if (rc != ILECALL_NOERROR) {
		return 0;
	}
	return 1;
}

/* {{{ 
*/
PHP_FUNCTION(ile_connect)
{
    /* return without exit */
	_VirtualInit();
    _RETURN();
}
/* }}} */

/*
 * match virt_h.rpgle
 */
#define VIRT_CHAR 1
#define VIRT_VARCHAR 2
#define VIRT_USC2 3
#define VIRT_VARUSC2 4
#define VIRT_GRAPH 5
#define VIRT_VARGRAPH 6
#define VIRT_IND 7
#define VIRT_PACKED 8
#define VIRT_ZONED 9
#define VIRT_BINDEC 10
#define VIRT_INT3 11
#define VIRT_INT5 12
#define VIRT_INT10 13
#define VIRT_LONG 13
#define VIRT_IN20 14
#define VIRT_UNS3 15
#define VIRT_UNS5 16
#define VIRT_UNS10 17
#define VIRT_UNS20 18
#define VIRT_FLOAT4 19
#define VIRT_FLOAT8 20
#define VIRT_DOUBLE 20
#define VIRT_DATE 21
#define VIRT_TIME 22
#define VIRT_TIMESTAMP 23
#define VIRT_POINTER 24
#define VIRT_POINTERPROC 25
#define VIRT_STRING 26

#define VIRT_IO_IN 1
#define VIRT_IO_OUT 2
#define VIRT_IO_BOTH 3

/* {{{ 
*/
PHP_FUNCTION(ile_callback)
{
	double iamd;
	int rc = 0, i = 0;
	int argc = ZEND_NUM_ARGS();
	char *argv[VIRT_CALLARG_MAX];
    char * name = (char *)NULL;
    int nameLen = 0;
    char * outval = (char *)NULL;
	zval * p[VIRT_CALLARG_MAX];
	virtVar_t desc[VIRT_CALLARG_MAX];
	/* up to 64 parms */
	if (zend_parse_parameters(argc TSRMLS_CC, 
	"s|zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz", &name, &nameLen,
	&p[0],&p[1],&p[2],&p[3],&p[4],&p[5],&p[6],&p[7],&p[8],&p[9],
	&p[10],&p[11],&p[12],&p[13],&p[14],&p[15],&p[16],&p[17],&p[18],&p[19],
	&p[20],&p[21],&p[22],&p[23],&p[24],&p[25],&p[26],&p[27],&p[28],&p[29],
	&p[30],&p[31]) == FAILURE) 
	{
		RETURN_FALSE;
		return;
	}
	/* make sure VirtualInit set */
	rc = _VirtualInit();
	for (i=0;i<VIRT_CALLARG_MAX;i++) {
		argv[i] = (char *) NULL;
        desc[i].iType = 0; 
		desc[i].iLen = 0; 
		desc[i].iScale = 0;
        desc[i].iTypeIO = VIRT_IO_IN;
        desc[i].paseAddr = 0;
	}

	/* input */
	for (i=0;i < argc - 1;i++) {
		switch (Z_TYPE_P(p[i])) {
		case IS_NULL:
		case IS_LONG:
        	desc[i].iType = VIRT_LONG; 
			desc[i].iLen = sizeof(i); 
  			desc[i].iScale = 0;
        	desc[i].iTypeIO = VIRT_IO_IN; 
  			desc[i].paseAddr = 0;
			argv[i] = (char *) &((p[i])->value.lval);
			break;
		case IS_DOUBLE:
        	desc[i].iType = VIRT_DOUBLE; 
			desc[i].iLen = sizeof(iamd); 
			desc[i].iScale = 0;
        	desc[i].iTypeIO = VIRT_IO_IN; 
  			desc[i].paseAddr = 0;
			argv[i] = (char *) &((p[i])->value.dval);
			break;
		case IS_STRING:
        	desc[i].iType = VIRT_STRING; 
			desc[i].iLen = Z_STRLEN_P(p[i]); 
			desc[i].iScale = 0;
        	desc[i].iTypeIO = VIRT_IO_IN; 
  			desc[i].paseAddr = 0;
			argv[i] = (char *) Z_STRVAL_P(p[i]);
			break;
		case IS_ARRAY:
			RETURN_FALSE;
			return;
			break;
		case IS_OBJECT:
			RETURN_FALSE;
			return;
			break;
/*
		case IS_FALSE:
		case IS_TRUE:
		case IS_RESOURCE:
		case IS_REFERENCE:
		case IS_UNDEF:
*/
		default:
			RETURN_FALSE;
			return;
			break;
		}
	}

	rc = _VirtualCallBack32(name, &desc[0], argc - 1, argv);


	/* output */
	for (i=0;i < argc - 1;i++) {
		outval = (char *)desc[i].paseAddr;
		switch (Z_TYPE_P(p[i])) {
		case IS_NULL:
		case IS_LONG:
			switch(desc[i].iTypeIO) {
			case VIRT_IO_OUT:
			case VIRT_IO_BOTH:
                /* Z_LVAL_P(p[i]) = *(long *) outval; */
			break;
			}
			break;
		case IS_DOUBLE:
			switch(desc[i].iTypeIO) {
			case VIRT_IO_OUT:
			case VIRT_IO_BOTH:
                /* Z_DVAL_P(p[i]) = *(double *) outval; */
			break;
			}
			break;
		case IS_STRING:
			switch(desc[i].iTypeIO) {
			case VIRT_IO_OUT:
			case VIRT_IO_BOTH:
				Z_STRVAL_P(p[i]) = estrndup(Z_STRVAL_P(p[i]), Z_STRLEN_P(p[i]));
				if (Z_STRLEN_P(p[i]) < desc[i].iLen) {
                	Z_STRVAL_P(p[i]) = erealloc(Z_STRVAL_P(p[i]), desc[i].iLen + 1);
					Z_STRLEN_P(p[i]) = desc[i].iLen;
				}
                strcpy(Z_STRVAL_P(p[i]), outval);
				break;
			}
			break;
		case IS_ARRAY:
			RETURN_FALSE;
			return;
			break;
		case IS_OBJECT:
			RETURN_FALSE;
			return;
			break;
/*
		case IS_FALSE:
		case IS_TRUE:
		case IS_RESOURCE:
		case IS_REFERENCE:
		case IS_UNDEF:
*/
		default:
			RETURN_FALSE;
			return;
			break;
		}
		if (desc[i].paseAddr) {
			free((char *)desc[i].paseAddr);
		}
	}

	RETURN_TRUE;
	return;
}
/* }}} */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: sw=4 ts=4 fdm=marker
 * vim<600: sw=4 ts=4
 */
